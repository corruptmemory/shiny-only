module gitlab.com/corruptmemory/shiny-only

go 1.16

require (
	dmitri.shuralyov.com/gpu/mtl v0.0.0-20201218220906-28db891af037
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb
	github.com/jezek/xgb v0.0.0-20210312150743-0e0f116e1240
	golang.org/x/image v0.0.0-20210504121937-7319ad40d33e
	golang.org/x/mobile v0.0.0-20210527171505-7e972142eb43
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea
)
