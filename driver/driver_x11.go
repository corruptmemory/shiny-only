// Copyright 2015 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build (linux && !android) || dragonfly || openbsd
// +build linux,!android dragonfly openbsd

package driver

import (
	"gitlab.com/corruptmemory/shiny-only/driver/x11driver"
	"gitlab.com/corruptmemory/shiny-only/screen"
)

func main(f func(screen.Screen)) {
	x11driver.Main(f)
}
